FROM balenalib/raspberrypi3-debian:buster

# Install dependencies
RUN apt-get update && \
  apt-get install -yq \
    python3 \
    python3-dev \
    python3-pip \
    python3-setuptools \
    gstreamer-1.0 \
    v4l-utils \
    libopus-dev \
    libvpx-dev \
    libsrtp2-dev \
    libopencv-dev \
    libatlas3-base \
    libatlas-base-dev \
    libjasper-dev \
    libilmbase23 \
    libopenexr23 \
    libavformat-dev \
    libswscale-dev \
    libqtgui4 \
    libqt4-test \
    libavdevice-dev \
    libavfilter-dev \
    libavcodec-dev \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

# Enable the v4l2 driver for the Raspberry Pi camera
#RUN printf "bcm2835-v4l2\n" >> /etc/modules
RUN pip3 install --upgrade pip setuptools wheel
RUN pip3 install async-timeout av --index-url https://www.piwheels.org/simple
RUN pip3 install -U aiohttp[speedups] aiohttp_basicauth aioice aiortc numpy opencv-python-headless opencv-contrib-python imutils scipy aiohttp_jwt --index-url https://www.piwheels.org/simple
RUN pip3 install hidapi pyluxafor --index-url https://www.piwheels.org/simple

WORKDIR /usr/src/app

COPY ./app/ /usr/src/app/

ENV UDEV=1

COPY udev-rules/ /etc/udev/rules.d/

CMD ["python3", "/usr/src/app/server.py"]
