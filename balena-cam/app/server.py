import asyncio, json, os, cv2, platform, sys
from time import sleep
import time
from aiohttp import web
from av import VideoFrame
from aiortc import RTCPeerConnection, RTCSessionDescription, VideoStreamTrack, RTCIceServer, RTCConfiguration
from aiortc.mediastreams import MediaStreamError
from aiohttp_basicauth import BasicAuthMiddleware
from aiohttp_jwt import JWTMiddleware

# Performance
from threading import Thread
from queue import Queue
from AtomicValue import AtomicValue
from imutils.video import WebcamVideoStream
from imutils.video import FPS
from remote_control import RemoteControl, pan_min, privacy_limit

#Luxafor
from pyluxafor import LuxaforFlag

black = (0, 0, 0)
white = (255, 255, 255)

device_index = int(os.environ.get("DEVICE_INDEX", 0))
videoStream = WebcamVideoStream(src=device_index).start()
device = videoStream.stream

arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_4X4_50)
arucoParams = cv2.aruco.DetectorParameters_create()

detectMarkers = os.environ.get('DETECT_MARKERS') != None

flag = LuxaforFlag()
try:
    flag.off()
except Exception as e:
    print("Error setting up luxafor:", e)
    flag = None

markerLabels = {
    0: "Wallflower",
    10: "Badges of",
    20: "Message panel",
    30: "receipt printer",
    40: "work",
}

pcs = set()

class PeerConnectionFactory():
    def __init__(self):
        self.config = {'sdpSemantics': 'unified-plan'}
        self.STUN_SERVER = None
        self.TURN_SERVER = None
        self.TURN_USERNAME = None
        self.TURN_PASSWORD = None
        if all(k in os.environ for k in ('STUN_SERVER', 'TURN_SERVER', 'TURN_USERNAME', 'TURN_PASSWORD')):
            print('WebRTC connections will use your custom ICE Servers (STUN / TURN).')
            self.STUN_SERVER = os.environ['STUN_SERVER']
            self.TURN_SERVER = os.environ['TURN_SERVER']
            self.TURN_USERNAME = os.environ['TURN_USERNAME']
            self.TURN_PASSWORD = os.environ['TURN_PASSWORD']
            iceServers = [
                {
                    'urls': self.STUN_SERVER
                },
                {
                    'urls': self.TURN_SERVER,
                    'credential': self.TURN_PASSWORD,
                    'username': self.TURN_USERNAME
                }
            ]
            self.config['iceServers'] = iceServers

    def create_peer_connection(self):
        if self.TURN_SERVER is not None:
            iceServers = []
            iceServers.append(RTCIceServer(self.STUN_SERVER))
            iceServers.append(RTCIceServer(self.TURN_SERVER, username=self.TURN_USERNAME, credential=self.TURN_PASSWORD))
            config = RTCConfiguration(iceServers)
            return RTCPeerConnection(config)
        return RTCPeerConnection()

    def get_ice_config(self):
        return json.dumps(self.config)

# Only marker-check check every nth frame
detect_frame = 10

class RTCVideoStream(VideoStreamTrack):
    def __init__(self, camera_device, index):
        super().__init__()
        self.camera_device = camera_device
        self.index = index
        self.last = 0
        self.fps = FPS().start()

        # Count the number of frames skipped before a marker check
        self.skip_count = 0

        self.corners = []
        self.ids = None

    async def recv(self):
        if self.readyState != "live":
            raise MediaStreamError

        frame = self.camera_device.read().copy()
        try:
            if self.fps._numFrames == 30:
                self.fps.stop()
                self.last = self.fps.fps()
                self.fps = FPS().start()
            else:
                self.fps.update()

            cv2.putText(frame, "[{}] FPS: {:.2f}".format(self.index, self.last), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, black, 3)
            cv2.putText(frame, "[{}] FPS: {:.2f}".format(self.index, self.last), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, white, 2)
        except Exception as e:
           print('fps calc', e)

        try:
            if detectMarkers:
                (_corners, _ids, _) = cv2.aruco.detectMarkers(frame, arucoDict, parameters=arucoParams)
                if len(_corners) > 0:
                    self.corners = _corners
                    self.ids = _ids
                    self.skip_count = 0

                # print("[INFO] Detect Frame: {}/{}".format(self.skip_count, detect_frame))
                if self.skip_count == detect_frame:
                    self.skip_count = 0
                    self.corners = []
                    self.ids = []
                else:
                    self.skip_count = self.skip_count + 1

            if len(self.corners) > 0:
                self.ids = self.ids.flatten()
                zipped = zip(self.corners, self.ids)

                # loop over the detected ArUCo corners
                for (markerCorner, markerID) in zipped:
                    label = markerLabels.get(markerID, str(markerID))
                    if self.skip_count == detect_frame:
                        print("[INFO] ArUco marker ID: {}".format(label))

                    # extract the marker corners (which are always returned in
                    # top-left, top-right, bottom-right, and bottom-left order)
                    (topLeft, topRight, bottomRight, bottomLeft) = markerCorner.reshape((4, 2))
                    # convert each of the (x, y)-coordinate pairs to integers
                    topRight = (int(topRight[0]), int(topRight[1]))
                    bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
                    bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
                    topLeft = (int(topLeft[0]), int(topLeft[1]))
                    cX = int((topLeft[0] + bottomRight[0]) / 2.0)
                    cY = int((topLeft[1] + bottomRight[1]) / 2.0)
                    cv2.circle(frame, (cX, cY), 4, (0, 0, 255), -1)

                    #background that looks like outline
                    cv2.putText(frame, label, (topLeft[0], topLeft[1] - 15), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 3)
                    cv2.putText(frame, label, (topLeft[0], topLeft[1] - 15), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
        except Exception as e:
           print('marker detection', e)


        frame = VideoFrame.from_ndarray(frame, format="bgr24")
        pts, time_base = await self.next_timestamp()
        frame.pts = pts
        frame.time_base = time_base
        return frame

async def index(request):
    content = open(os.path.join(ROOT, 'client/index.html'), 'r').read()
    return web.Response(content_type='text/html', text=content)

async def stylesheet(request):
    content = open(os.path.join(ROOT, 'client/style.css'), 'r').read()
    return web.Response(content_type='text/css', text=content)

async def javascript(request):
    content = open(os.path.join(ROOT, 'client/client.js'), 'r').read()
    return web.Response(content_type='application/javascript', text=content)

async def balena(request):
    content = open(os.path.join(ROOT, 'client/balena-cam.svg'), 'r').read()
    return web.Response(content_type='image/svg+xml', text=content)

async def balena_logo(request):
    content = open(os.path.join(ROOT, 'client/balena-logo.svg'), 'r').read()
    return web.Response(content_type='image/svg+xml', text=content)

async def favicon(request):
    return web.FileResponse(os.path.join(ROOT, 'client/favicon.ico'))

async def handle_401(request):
    content = open(os.path.join(ROOT, 'client/accessdenied.html'), 'r').read()
    return web.Response(status=401, content_type='text/html', text=content)

async def handle_403(request):
    content = open(os.path.join(ROOT, 'client/accessdenied.html'), 'r').read()
    return web.Response(status=403, content_type='text/html', text=content)

async def viewers(request):
    count = len(pcs)
    return web.Response(
        content_type='application/json',
        text=json.dumps({
            'count': count,
        }))

async def offer(request):
    params = await request.json()
    offer = RTCSessionDescription(
        sdp=params['sdp'],
        type=params['type'])
    pc = pc_factory.create_peer_connection()
    pcs.add(pc)
    count = len(pcs)
    print("[INFO] PeerConnections: {}".format(count))
    if flag and count > 0:
        flag.do_static_colour(count, 255, 0, 0)

    local_video = RTCVideoStream(videoStream, len(pcs))
    pc.addTrack(local_video)

    @pc.on("connectionstatechange")
    async def on_connectionstatechange():
        if pc.connectionState == "failed":
            await pc.close()
            pcs.discard(pc)
            count = len(pcs)
            print("[INFO] PeerConnections: {}".format(count))
            if flag and count == 0:
                flag.off()

    @pc.on('datachannel')
    def on_datachannel(channel):
        channel.send(json.dumps({
            'pan': device.get(cv2.CAP_PROP_PAN),
            'tilt': device.get(cv2.CAP_PROP_TILT),
            'zoom': device.get(cv2.CAP_PROP_ZOOM),
        }))

        @channel.on('message')
        def on_message(message):
            if device.get(cv2.CAP_PROP_TILT) > privacy_limit:
                try:
                    params = json.loads(message)
                    if 'pan' in params:
                        pan = int(params['pan'])
                        if pan > pan_min:
                            device.set(cv2.CAP_PROP_PAN, pan)

                    if 'tilt' in params:
                        tilt = int(params['tilt'])
                        device.set(cv2.CAP_PROP_TILT, tilt)

                    if 'zoom' in params:
                        zoom = int(params['zoom'])
                        device.set(cv2.CAP_PROP_ZOOM, zoom)
                except Exception as e:
                    print('ptz', e)

            channel.send(json.dumps({
                'pan': device.get(cv2.CAP_PROP_PAN),
                'tilt': device.get(cv2.CAP_PROP_TILT),
                'zoom': device.get(cv2.CAP_PROP_ZOOM),
            }))

    await pc.setRemoteDescription(offer)
    answer = await pc.createAnswer()
    await pc.setLocalDescription(answer)
    return web.Response(
        content_type='application/json',
        text=json.dumps({
            'sdp': pc.localDescription.sdp,
            'type': pc.localDescription.type
        }))

async def config(request):
    return web.Response(
        content_type='application/json',
        text=pc_factory.get_ice_config()
    )

async def on_shutdown(app):
    # close peer connections
    coros = [pc.close() for pc in pcs]
    await asyncio.gather(*coros)

async def get_token(request):
    return request.cookies.get('nf_jwt')

def create_error_middleware(overrides):
    @web.middleware
    async def error_middleware(request, handler):
        try:
            return await handler(request)
        except web.HTTPException as ex:
            override = overrides.get(ex.status)
            if override:
                return await override(request)

            raise
        except Exception:
            request.protocol.logger.exception("Error handling request")
            raise

    return error_middleware

if __name__ == '__main__':
    ROOT = os.path.dirname(__file__)

    if 'REMOTE_CONTROL' in os.environ:
        remote_control = RemoteControl(device)
        remote_control.run()
    else:
        remote_control = None

    error_middleware = create_error_middleware({
        401: handle_401,
        403: handle_403,
    })

    auth = [error_middleware]
    if 'username' in os.environ and 'password' in os.environ:
        print('\n#############################################################')
        print('Authorization is enabled.')
        print('Your balenaCam is password protected.')
        print('#############################################################\n')
        auth.append(BasicAuthMiddleware(username = os.environ['username'], password = os.environ['password']))
    if 'jwt_secret' in os.environ:
        print('\n#############################################################')
        print('Authorization is enabled.')
        print('Your balenaCam is jwt protected.')
        print('#############################################################\n')
        auth.append(JWTMiddleware(
            secret_or_pub_key=os.environ['jwt_secret'],
            token_getter=get_token,
            request_property='jwt',
            algorithms=['HS256'],
            whitelist=[r"/accessdenied*"],
        ))
    else:
        print('\n#############################################################')
        print('Authorization is disabled.')
        print('Anyone can access your balenaCam, using the device\'s URL!')
        print('Set the username and password environment variables \nto enable authorization.')
        print('For more info visit: \nhttps://github.com/balena-io-playground/balena-cam')
        print('#############################################################\n')

    # Factory to create peerConnections depending on the iceServers set by user
    pc_factory = PeerConnectionFactory()

    app = web.Application(middlewares=auth)
    app.on_shutdown.append(on_shutdown)
    app.router.add_get('/', index)
    app.router.add_get('/favicon.ico', favicon)
    app.router.add_get('/balena-logo.svg', balena_logo)
    app.router.add_get('/balena-cam.svg', balena)
    app.router.add_get('/client.js', javascript)
    app.router.add_get('/style.css', stylesheet)

    app.router.add_post('/offer', offer)
    app.router.add_get('/viewers', viewers)
    app.router.add_get('/ice-config', config)
    if os.getuid() == 0:
        web.run_app(app, port=80)
    else:
        web.run_app(app, port=9000)

    if remote_control:
        remote_control.wait()
