import React, { useState, useEffect, useRef, useCallback } from "react";

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Spinner from 'react-bootstrap/Spinner';
import Form from 'react-bootstrap/Form';
import Alert from 'react-bootstrap/Alert';

import { Joystick } from 'react-joystick-component';

import {When} from 'react-if';

import { useHotkeys } from 'react-hotkeys-hook';

import './App.css';
import logo from './balena-cam.svg';

const { NODE_ENV } = process.env

const { location } = window;
const myUrl = new URL(location);
const { searchParams } = myUrl;
const controlOnly = searchParams.get('controlOnly') === 'true';

const panStep = 3600;
const panMax = 352548;
const panMin = -468000;

const tiltStep = 7200;
const tiltMax = 324000;
const tiltMin = -240000;

const tiltPrivacyLimit = -250000

const POIs = [];

const isSafari = !!navigator.userAgent.match(/Version\/[\d]+.*Safari/);
const iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
const safariOnIos = isSafari && iOS;

async function iceGatheringComplete(pc) {
  // wait for ICE gathering to complete
  return new Promise(function(resolve) {
    if (pc.iceGatheringState === 'complete') {
      resolve();
    } else {
      function checkState() {
        if (pc.iceGatheringState === 'complete') {
          pc.removeEventListener('icegatheringstatechange', checkState);
          resolve();
        }
      }
      pc.addEventListener('icegatheringstatechange', checkState);
    }
  });
}

function App() {
  const videoElem = useRef(null);
  const [state, setState] = useState(0);
  const [iceConnectionState, setIceConnectionState] = useState();
  const [kind, setKind] = useState('spinner');
  const [isVideoAttached, setIsVideoAttached] = useState(false);
  const [viewers, setViewers] = useState(0);

  const primaryPeerConnection = useRef(null);
  const datachannel = useRef(null);

  const [pan, setPan] = useState(0);
  const [tilt, setTilt] = useState(0);
  const [zoom, setZoom] = useState(0);

  const [destinationTilt, setDestinationTilt] = useState(0);
  const [destinationPan, setDestinationPan] = useState(0);
  const [following, setFollowing] = useState(true);
  const svgElem = useRef(null);

  const goTo = useCallback((x, y, newZoom = null) => {
    if (datachannel.current === null) {
      console.warn('cannot move camera, no data channel')
      return;
    }
    console.log('goTo', {x, y, newZoom});

    const needsPan = Math.abs(pan - x) > panStep
    const needsTilt = Math.abs(tilt - y) > tiltStep
    const needsZoom = newZoom && Math.abs(zoom - newZoom) > 5
    const movement = {}

    if (needsPan) {
      movement['pan'] = x
    }
    if (needsTilt) {
      movement['tilt'] = y
    }
    if (needsZoom) {
      movement['zoom'] = newZoom
    }

    datachannel.current.send(JSON.stringify(movement));
  }, [pan, tilt, zoom]);

  useEffect(() => {
    async function updateViewers() {
      const response = await fetch('/viewers', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        },
      });
      if (response.ok) {
        const data = await response.json();
        const { count } = data;
        setViewers(count);
      } else {
        setViewers(0);
      }
    }
    setTimeout(updateViewers, 30 * 1000);
    updateViewers();
  }, []);

  useEffect(() => {
    if (tilt < tiltPrivacyLimit) {
      setKind('privacy')
    } if (isVideoAttached) {
      setKind('video')
    }
  }, [tilt, isVideoAttached])

  useEffect(() => {
    if(following) {
      setDestinationPan(pan);
      setDestinationTilt(tilt);
    }
  }, [following, pan, tilt])

  useEffect(() => {
    if(!following) {
      goTo(destinationPan, destinationTilt);
    }
  }, [following, destinationPan, destinationTilt, goTo])

  const eventName = safariOnIos ? 'pagehide' : 'beforeunload';
  window.addEventListener(eventName, function (event) {
    if (primaryPeerConnection.current !== null) {
      primaryPeerConnection.current.close();
    }
  });

  useEffect(() => {
    console.warn({iceConnectionState})
    function attachStreamToVideoElement(){
      const pc = primaryPeerConnection.current

      console.log('Attaching stream...');
      const srcStream = new MediaStream();
      const receivers = pc.getReceivers()
      srcStream.addTrack(receivers[0].track);
      if (videoElem.current) {
        videoElem.current.srcObject = srcStream;
        setIsVideoAttached(true);
        setKind('video');
        if (state === 0) {
          setState(1);
        }

      } else {
        console.log('no videoElem')
        if (primaryPeerConnection.current) {
          primaryPeerConnection.current.close();
          primaryPeerConnection.current = null;
        }
        setKind('fail');
      }
    }

    switch(iceConnectionState) {
      case 'connected':
      case 'completed':
        if (!isVideoAttached && !controlOnly) {
          attachStreamToVideoElement();
        }
        break;
      //bad
      case 'disconnected':
      case 'failed':
      case 'closed':
        if (state !== 2) {
          setKind('fail');
        }
        break;
      default:
        break;
    }
  }, [state, isVideoAttached, iceConnectionState]);

  function setupDataChannel(pc) {
    if (datachannel.current) {
      console.log('datachannel already open');
      datachannel.current.close();
      datachannel.current = null;
    }

    console.log('setupDataChannel')
    var heartbeat;
    let dc = pc.createDataChannel('ptz', {protocol: "json"});

    dc.onclose = function() {
      clearInterval(heartbeat);
      console.log('data -', 'close');
      datachannel.current = null
    };

    dc.onerror = console.log

    dc.onopen = function() {
      console.log('data -', 'open');
      datachannel.current = this;
      this.send('{}');

      if (heartbeat) {
        console.log('only one heartbeat')
      } else {
        heartbeat = setInterval(() => {
          this.send('{}');
        }, 1000);
      }
    };

    dc.onmessage = function (evt) {
      // console.log('data <', evt.data);
      try {
        const message = JSON.parse(evt.data);
        const { pan, tilt, zoom } = message;
        //console.log({pan, tilt, zoom})
        if (pan) {
          setPan(pan)
        }
        if (tilt) {
          setTilt(tilt)
        }
        if (zoom) {
          setZoom(zoom)
        }
      } catch (e) {
        console.log('onmessage', e)
      }
    };
  }

  useEffect(() => {
    async function createNewPeerConnection(config) {
      setIsVideoAttached(false);

      const pc = new RTCPeerConnection(config);
      setupDataChannel(pc);

      pc.addEventListener('iceconnectionstatechange', () => {setIceConnectionState(pc.iceConnectionState)})

      pc.addTransceiver('video', {direction: 'recvonly'});
      try {
        const offer = await pc.createOffer()
        await pc.setLocalDescription(offer);
        await iceGatheringComplete(pc)

        const { localDescription } = pc
        const { sdp, type } = localDescription;
        console.log('Offer SDP');
        // console.log(sdp);

        const response = await fetch('/offer', {
          method: 'POST',
          body: JSON.stringify({sdp, type}),
          headers: {
            'Content-Type': 'application/json'
          },
        });
        const answer = await response.json();
        // console.log(answer.sdp);
        await pc.setRemoteDescription(answer);

      } catch(e) {
        console.error('createNewPeerConnection', e);
        if (primaryPeerConnection.current) {
          primaryPeerConnection.current.close();
          primaryPeerConnection.current = null;
        }
        setKind('fail');
      }
      return pc;
    }

    async function fetchIce() {
      try {
        const response = await fetch('/ice-config');
        if (response.ok) {
          const config = await response.json();
          primaryPeerConnection.current = await createNewPeerConnection(config);
        } else if (response.status === 401) {
          setKind('accessdenied');
        } else if (response.status === 403) {
          setKind('accessdenied');
        } else if (response.status === 503) {
          setKind('offline');
        }
      } catch(e) {
        console.error('Error while getting the ICE server configuration');
        console.error(e);
        setKind('fail');
      }
    }

    if (videoElem.current) {
      fetchIce();
    } else {
      console.log('no video element')
    }
  }, []);

  function onZoom(event){
    const { target } = event
    const { valueAsNumber } = target
    if (valueAsNumber >= 0 && valueAsNumber <= 100) {
      const msg = JSON.stringify({
        zoom: valueAsNumber,
      })
      datachannel.current.send(msg);
    }
  }

  useHotkeys('up', () => {
    datachannel.current.send(JSON.stringify({tilt: tilt + tiltStep * 2}));
  }, [tilt])
  useHotkeys('down', () => {
    datachannel.current.send(JSON.stringify({tilt: tilt - tiltStep * 2}));
  }, [tilt])
  useHotkeys('left', () => {
    datachannel.current.send(JSON.stringify({pan: pan + panStep * 2}));
  }, [pan])
  useHotkeys('right', () => {
    datachannel.current.send(JSON.stringify({pan: pan - panStep * 2}));
  }, [pan])

  function handleStop() {
    goTo(destinationPan, destinationTilt)
    setFollowing(true);
  }

  function handleMove(event){
    setFollowing(false);
    const { type, direction } = event;
    if (type === 'move') {
      switch(direction) {
        case 'FORWARD':
          setDestinationTilt(destinationTilt + tiltStep);
          break;
        case 'LEFT':
          setDestinationPan(destinationPan + panStep);
          break;
        case 'RIGHT':
          setDestinationPan(destinationPan - panStep);
          break;
        case 'BACKWARD':
          setDestinationTilt(destinationTilt - tiltStep);
          break;
        default:
          break;
      }
    }
  }

  const viewWidth = (100 - zoom) * 0.01 * 275760
  const viewHeight = (100 - zoom) * 0.01 * 206820

  function oMousePosSVG(e) {
    if(svgElem.current) {
      const p = svgElem.current.createSVGPoint();
      p.x = e.clientX;
      p.y = e.clientY;
      const ctm = svgElem.current.getScreenCTM().inverse();
      return p.matrixTransform(ctm);
    }
  }

  function svgClick(event) {
    const { x, y } = oMousePosSVG(event);
    goTo(-x, -y);// Correct for inverse scale
  }

  return (
    <>
      <Navbar collapseOnSelect expand="sm" bg="dark" variant="dark">
        <Navbar.Brand><img alt="balena logo" style={{width: '13rem', height: 'auto'}} src={logo}/></Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Item>
              <Nav.Link href="/?controlOnly=true">Controls Only</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Navbar.Text>{viewers} Viewers</Navbar.Text>
            </Nav.Item>
          </Nav>
          <Nav>
            <When condition={primaryPeerConnection.current !== null}>
              <Navbar.Text>
                Connection over WebRTC
              </Navbar.Text>
              &nbsp;
              <Navbar.Text>
                <i className="fa fa-video-camera" style={{fontSize: '150%'}}></i>
              </Navbar.Text>
            </When>
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      <div className="main-container">
        <When condition={kind === 'spinner' && primaryPeerConnection.current === null}>
          <Container className="mt-5">
            <Row className="justify-content-center align-items-center text-center">
              <Spinner animation="border" role="status"/>
            </Row>
            <Row className="justify-content-center align-items-center text-center">
              <h4>Connecting...</h4>
            </Row>
            <Row className="justify-content-center align-items-center text-center">
              <h6>Please wait</h6>
            </Row>
          </Container>
        </When>

        <When condition={kind === 'privacy'}>
          <Container className="mt-5">
            <Row className="justify-content-center align-items-center text-center">
              <Alert>Privacy mode</Alert>
            </Row>
          </Container>
        </When>

        <Container className={kind === 'video' ? '' : 'd-none'}>
          <Row className="justify-content-center align-items-center text-center">
            <Col className="embed-responsive embed-responsive-16by9 position-relative">
              <video ref={videoElem} muted className="embed-responsive-item" autoPlay playsInline></video>
            </Col>
          </Row>
        </Container>

        <Container className={datachannel.current !== null ? '' : 'd-none'}>
          <Row className="justify-content-center align-items-center text-center">
            <Col className={NODE_ENV === 'development' ? '' : 'd-none'}>Pan: {pan}, Tilt: {tilt}, Zoom: {zoom}</Col>
            <Col className={NODE_ENV === 'development' ? '' : 'd-none'}>DestinationPan: {destinationPan}, DestinationTilt: {destinationTilt}</Col>
          </Row>
          <Row className="justify-content-center align-items-center text-center">
            <Col>
              <svg ref={svgElem} height="100%" width="100%" viewBox={`${panMin} ${tiltMin} ${panMax - panMin} ${tiltMax - tiltMin}`} xmlns="http://www.w3.org/2000/svg" onClick={svgClick}>
                <g transform="scale(-1,-1)">
                  {POIs.map((poi, index) => {
                    const {x, y, zoom, name, color} = poi;
                    const width = (100 - zoom) * 0.01 * 275760
                    const height = (100 - zoom) * 0.01 * 206820
                    return (
                      <g key={index} transform={`translate(${-width/2},${-height/2})`} onClick={() => goTo(x, y, zoom)} style={{cursor: 'zoom-in'}}>
                        <rect
                          x={x} y={y} width={width} height={height}
                          stroke={color}
                          strokeWidth={500}
                          fillOpacity={0}
                          strokeOpacity={datachannel.current === null ? 0 : 1}
                        />
                        <g transform={`translate(${x+width}, ${y+height/2}) scale(-1, -1)`}>
                          <text x={0} y={0}
                            fill="black"
                            fillOpacity={datachannel.current === null ? 0 : 1}
                            stroke="black"
                            strokeOpacity={datachannel.current === null ? 0 : 1}
                            fontSize={24}
                            transform={`scale(1000, 1000)`}
                          >{name}</text>
                        </g>
                      </g>
                    );
                  })}

                  <g
                    strokeOpacity={datachannel.current === null ? 0 : 1} fillOpacity={datachannel.current === null ? 0 : 1}
                    style={{pointerEvents: 'none'}}
                  >
                    <rect
                      x={pan}
                      y={tilt}
                      width={viewWidth}
                      height={viewHeight}
                      stroke="black"
                      strokeWidth={500}
                      transform={`translate(${-viewWidth/2},${-viewHeight/2})`}
                      fillOpacity={0}
                    />

                    <circle cx={destinationPan} cy={destinationTilt} r={(100 - zoom) * 0.01 * 10000} fill="red"/>

                    <line x1={destinationPan - viewWidth/2} y1={destinationTilt} x2={panMin} y2={destinationTilt} stroke="black" fill="black" strokeWidth="300" />
                    <line x1={destinationPan + viewWidth/2} y1={destinationTilt} x2={panMax} y2={destinationTilt} stroke="black" fill="black" strokeWidth="300" />

                    <line x1={destinationPan} y1={destinationTilt - viewHeight/2} x2={destinationPan} y2={tiltMin} stroke="black" fill="black" strokeWidth="300" />
                    <line x1={destinationPan} y1={destinationTilt + viewHeight/2} x2={destinationPan} y2={tiltMax} stroke="black" fill="black" strokeWidth="300" />
                  </g>
                </g>
              </svg>
            </Col>
          </Row>
          <Row className="justify-content-center align-items-center text-center">
            <Col>
              <Joystick stickColor={datachannel.current === null ? "grey" : "blue"} throttle={100} move={handleMove} stop={handleStop} disabled={datachannel.current === null} ></Joystick>
            </Col>
            <Col>
              <Form.Group>
                <Form.Label>Zoom ({zoom})</Form.Label>
                <Form.Range min={0} max={100} step={1} onChange={onZoom} value={zoom} disabled={datachannel.current === null} />
              </Form.Group>
            </Col>
          </Row>
        </Container>

        <When condition={kind === 'fail'}>
          <Container>
            <Row className="justify-content-center align-items-center text-center">
              <h4>Connection failed.</h4>
            </Row>
            <Row className="justify-content-center align-items-center text-center">
              <h6>
                Sorry!
                <span style={{fontSize: '150%'}}>
                  <b>  :( </b>
                </span>
              </h6>
            </Row>
          </Container>
        </When>

        <When condition={kind === 'offline'}>
          <Container>
            <Row className="justify-content-center align-items-center text-center">
              <Alert variant="secondary">
                Offline
              </Alert>
            </Row>
          </Container>
        </When>

        <When condition={kind === 'accessdenied'}>
          <Container>
            <Row className="justify-content-center align-items-center text-center">
              <h1><code>Access Denied</code></h1>
              <h3>🚫🚫🚫🚫</h3>
              <h6>Generate URL using <a href="https://accessgranted.ericbetts.dev/">Access Granted</a> card to gain access</h6>
            </Row>
          </Container>
        </When>
      </div>
    </>
  );
}

export default App;
