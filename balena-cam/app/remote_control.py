import cv2
import hid
from threading import Thread

privacy_limit = -250000
pan_step = 3600
tilt_step = 7200
pan_min = -352548

class RemoteControl():
    def __init__(self, camera):
        self.camera = camera
        self.thread = None
        try:
            self.h = hid.device()
            self.h.open(0x348f, 0x1010)

        except IOError as ex:
            print("Error setting up remote control:", ex)
            self.h = None

    def run(self):
        if self.h == None:
            self.thread = None
            return
        self.thread = Thread(target=self.watchHID)
        self.thread.start()

    def wait(self):
        if self.thread:
            try:
                print("Join threads for RemoteControl")
                self.thread.join()
            except KeyboardInterrupt:
                exit()

    def watchHID(self):
        actions = {
            15: self.power,
            #23: self.track,
            #30: self.p1,
            #31: self.p2,
            #32: self.p3,
            4: self.left,
            7: self.right,
            26: self.up,
            22: self.down,
            21: self.reset,
            #19: self.prev_dev,
            #17: self.next_dev,
            18: self.zoom_out,
            12: self.zoom_in,
        }
        done = False
        while not done:
            try:
                self.h.set_nonblocking(1)
                d = self.h.read(64)
                if d:
                    if len(d) != 8:
                        continue

                    if d[2] == 0: # Key up event
                        continue

                    action = actions.get(d[2])
                    if action:
                        action()
                    else:
                        print("no action for", d[2])

            except KeyboardInterrupt:
                print("setting done")
                done = True

    def zoom_in(self):
        current = self.camera.get(cv2.CAP_PROP_ZOOM)
        self.camera.set(cv2.CAP_PROP_ZOOM, current + 5)
    def zoom_out(self):
        current = self.camera.get(cv2.CAP_PROP_ZOOM)
        self.camera.set(cv2.CAP_PROP_ZOOM, current - 5)

    def left(self):
        current = self.camera.get(cv2.CAP_PROP_PAN)
        self.camera.set(cv2.CAP_PROP_PAN, current + pan_step * 2)
    def right(self):
        current = self.camera.get(cv2.CAP_PROP_PAN)
        self.camera.set(cv2.CAP_PROP_PAN, current - pan_step * 2)
    def up(self):
        current = self.camera.get(cv2.CAP_PROP_TILT)
        self.camera.set(cv2.CAP_PROP_TILT, current + tilt_step * 2)
    def down(self):
        current = self.camera.get(cv2.CAP_PROP_TILT)
        self.camera.set(cv2.CAP_PROP_TILT, current - tilt_step * 2)
    def reset(self):
        self.camera.set(cv2.CAP_PROP_PAN, 0)
        self.camera.set(cv2.CAP_PROP_TILT, 0)
        self.camera.set(cv2.CAP_PROP_ZOOM, 0)

    def power(self):
        current = self.camera.get(cv2.CAP_PROP_TILT)
        if current > privacy_limit:
            print("on -> off")
            self.camera.set(cv2.CAP_PROP_TILT, privacy_limit - tilt_step * 2)
        else:
            print("off -> on")
            self.camera.set(cv2.CAP_PROP_TILT, 0)


