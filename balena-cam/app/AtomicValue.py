from threading import RLock

class AtomicValue(object):
    def __init__(self, value):
        self._lock = RLock()
        self._value = value

    @property
    def value(self):
        with self._lock:
            return self._value

    @value.setter
    def value(self, value):
        with self._lock:
            self._value = value

