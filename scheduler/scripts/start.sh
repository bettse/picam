#!/bin/bash
if [ -n "$ENABLE_CAMERA_TIMER" ]
then
  (crontab -l; echo "${CAMERA_ON:-0 8 * * *} /usr/src/camera_on.sh") | crontab -
  (crontab -l; echo "${CAMERA_OFF:-0 23 * * *} /usr/src/camera_off.sh") | crontab -
fi

if [ -n "$TIMEZONE" ]
then
  echo "${TIMEZONE}" > /etc/timezone
  cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
fi

crond -f
